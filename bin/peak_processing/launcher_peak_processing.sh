#!/bin/bash

# Will loop through all data sets in the input folder (passed as first argument) and
# process the peaks for each of the subfolders in the input folder. It assumes the datasets
# are first level subfolders in the input directory. Specify an output folder as the 
# second argument. The peak caller used should be specified as the 3rd paramter, the 
# prediction model of interest (PWM, DiMO, BEM, TFFM, DNA or ALL) should be specified
# as the 4th parameter and optional arguments follow. The number of processes to be 
# launched in parallel depends on the 5th argument. The rest of the arguments are used
# for using custom PFMs/TF names and/or window sizes for matching TFBSs and/or the 
# size of the sequences used for training the prediction models (DiMO, TFFM, DNA). 
#
# Dependencies:
# -------------
# bin/peak_processing/peak_processing.sh
# bin/config.sh
#
# NOTE that it assumes that the peak_processing script is located in the same folder as this script.
# If this is not the case, modify the config.sh file accordingly
# 
# IMPORTANT: a complete list of dependencies is in peak_processing.sh and READ.ME
#
# More details about the processing are in the header of the peak_processing.sh file and in the READ.ME
#
# load the paths of all dependencies 
# NOTE: it assumes that the config.sh file is in the bin folder of the pipeline
source "../config.sh"

usage="usage: bash launch_peak_processing.sh /my/input/folder /my/output/folder/ MACS DiMO 20 [my_pfm] [my_tf_name] [window_size] [training_window_size] "

#parse arguments
in_dir=;
if [ -z "$1" ]
    then 
	echo "No input file folder specified. Exiting..."; 
        echo "$usage"
	exit 1
    else	
	in_dir=$1
fi

out_dir=;
if [ -z "$2" ]
    then
        echo "No output folder specified. Exiting..."
        echo "$usage"
        exit 1
    else
        out_dir=$2
fi

data_type=;
if [ -z "$3" ]
    then
        echo "No data type specified. Use MACS, HOMER, BCP or GEM. Exiting..."
        echo "$usage"
        exit 1
    else
        data_type=$3
fi


# which prediction model to be used in processing
model=;
if [ -z "$4" ]
    then
        echo "No model specified. Exiting..."
        echo "$usage"
        exit 1
    else
        model=$4
fi

#get how many processors to use in parallel for processing
processors=;
if [ -z "$5" ]
    then
        echo "No maximum number of processors to use was specified. Using 1 processor...";
        processors=1
    else
        processors=$5
fi

# optional arguments
pfm=;
if [ ! -z "$6" ]
    then
        pfm=$6
fi

tf_name=;
if [ ! -z "$7" ]
    then
        tf_name=$7
fi

window_size=;
if [ ! -z "$8" ]
    then
        window_size=$8
fi

training_window_size=;
if [ ! -z "$9" ]
    then
        training_window_size=$9
fi

# count how many folder we got to process
nb_folders="$(ls -l $in_dir | grep -c ^d)"

#print parameter settings
echo "---------------------------------------"
echo "Input folder: $in_dir"
echo "Output folder: $out_dir"
echo "Data type: $data_type"
echo "Prediction model: $model"
echo "Window size: $window_size"
echo "Nb. folders to process: $nb_folders"
echo "Nb. processors: $processors"
echo "---------------------------------------"


# LAUNCH PROCESSING

# get all subfolders at depth 1
find $in_dir -maxdepth 1 -mindepth 1 -type d | xargs -I {} --max-proc=$processors bash $PEAK_PROCESSING_SCRIPT {} $out_dir $data_type $model $pfm $tf_name $window_size $training_window_size
