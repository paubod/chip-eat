filter_sequences = function(input_file = NULL, output_file = NULL, read_length = 101){

  fasta = read.table(input_file, sep = "\t", header = F)
  
  header = ""
  sink(output_file)
  for (i in 1:nrow(fasta)){
    line = as.character(fasta[i,1])
    if (grepl(">", line)){
        #the header is just a place holder - not really used
        header = line
    }else{
        #make sure that each line will contain the required number of nucleotides
        if (!grepl("N", line, ignore.case = T) && nchar(line) == read_length){
          cat(paste(header,"\n", sep = ""))
          cat(paste(line, "\n", sep = ""))
        }
    }
  }
  sink()
}
