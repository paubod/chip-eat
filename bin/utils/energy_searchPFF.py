import os
import sys
import pandas as pd
from math import exp

def get_motif_from_meme(meme, motif="MOTIF"):
    """
    Extract a motif from meme file given a unique motif
    name and create dictionary for sequence scoring

    Default motif name is keyword MOTIF for single motif files. 
    """

    pwm_dictionary = {}
    pwm_dictionary["A"] = []
    pwm_dictionary["C"] = []
    pwm_dictionary["G"] = []
    pwm_dictionary["T"] = []
    flag = 0
    check = 0
    with open(meme, "r") as f1:
        for line in f1:
            if str(motif).upper() in line.upper():
                flag += 1
            if "letter-probability" in line and flag == 1:
                w = line.split(" ")[5]
                flag += 1
                continue
            if flag == 2 and int(check) < int(w):
                if line == "\n":
                    continue
                else:
                    words = line.split()
                    pwm_dictionary["A"].append(float(words[0]))
                    pwm_dictionary["C"].append(float(words[1]))
                    pwm_dictionary["G"].append(float(words[2]))
                    pwm_dictionary["T"].append(float(words[3]))
                    check += 1
        return pwm_dictionary, int(w)


def rc_pwm(area_pwm, pwm_len):
    """
    Takes as input the forward pwm and returns a reverse
    complement of the motif
    """
    rcareapwm = {}
    rcareapwm["A"] = []
    rcareapwm["C"] = []
    rcareapwm["G"] = []
    rcareapwm["T"] = []

    for i in range(pwm_len):
        rcareapwm["A"].append(area_pwm["T"][pwm_len - i - 1])
        rcareapwm["C"].append(area_pwm["G"][pwm_len - i - 1])
        rcareapwm["G"].append(area_pwm["C"][pwm_len - i - 1])
        rcareapwm["T"].append(area_pwm["A"][pwm_len - i - 1])
        
    return rcareapwm

def energyscore(pwm_dictionary, seq):
    """
    Score sequences using the beeml energy scoring approach.

    Borrowed greatly from the work of Zhao and Stormo

    P(Si)=1/(1+e^Ei-u)

    Ei=sumsum(Si(b,k)e(b,k))

    Previous approaches seem to be using the the minimum sum of the
    energy contribution of each of the bases of a specific region.

    This is currently showing some promise but further testing is
    needed to ensure that I have a robust algorithm. - failed

    20170329 - MG: corrected to loop over the entire PFM and not
    omit the last nucleotide. Loop over entire sequence stops before
    the length of the PFM to avoid adding useless energies for non-
    existent nucleotides.

    NOTES: 1)that the energy score is now (1- min(energy)) * 100 
    to avoid further modifications in the downstream analysis
          2) that the start of the top scoring sequence is 1 based 

    """
    pwm_length = len(pwm_dictionary["A"])
    energy_list = []
    sequence_list = [] 
    start_index_list = []
    pwm_dictionary_rc = rc_pwm(pwm_dictionary, pwm_length)
    for i in range(len(seq) - pwm_length):
        energy = 0
        energy_rc = 0
        sequence = ""
        sequence_rc = ""
        for j in range(pwm_length):
            nucl = seq[j + i]
            #check if nucleotide is valid and not N or other letter
            if not is_valid(nucl, ['A', 'C', 'G', 'T']):
                #or skip until next nucleotide in the sequence
                i += (j + 1)
                break
            energy += pwm_dictionary[nucl][j]
            energy_rc += pwm_dictionary_rc[nucl][j]

            sequence += nucl
            sequence_rc += nucl
        
        # store energy information
        energy_list.append(1 / (1 + (exp(energy))))
        energy_list.append(1 / (1 + (exp(energy_rc))))
      
        # store sequence information
        sequence_list.append(sequence)
        sequence_list.append(get_rc(sequence_rc))

        # store the start positions relative to start of the sequence
        start_index_list.append(i)
        start_index_list.append(i)
           
    index_min = energy_list.index(min(energy_list))
    strand = "+" if (index_min % 2 == 0) else "-"
     
    return strand, (1 - energy_list[index_min]) * 100, start_index_list[index_min] + 1, (start_index_list[index_min] + pwm_length), sequence_list[index_min]

def is_valid(nucleotide, lst):
    return any(nucleotide in word for word in lst)

rc = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'}
def get_rc(seq):
    """
    Returns the reverse complement of the sequence
    passed as parameter
    """
    seq_rc = ""
    for i in range(len(seq)):
        seq_rc += rc[seq[len(seq)-1-i]]

    return seq_rc

#############################################################################
# Run the complete motif assessment as a function
#############################################################################

def run(chip_seq_data, motif_details, results_path, tf, output_file = ""):

    motif_details = get_motif_from_meme(motif_details, tf)
    if not os.path.exists(results_path):
        os.makedirs(results_path)
    if not args.outfile:
	output_file=args.out + "/" + os.path.basename(args.data) + "_" + os.path.splitext(os.path.basename(args.pfm))[0] + ".bem"
    else:
        output_file=args.outfile
    
    with open(output_file, "w") as raw_out:
        
        #read data and score sequences
        input_file = pd.read_table(chip_seq_data, header=None)
        genomic_location = input_file[0]
        seq_score = input_file[1].apply(lambda seq: energyscore(motif_details[0], seq.upper()))

        #output results
        write_out_data=""
        for i in range(len(input_file)):
            #added 20171022 bt MG. Using BCP peak caller caused to output empty sequences
            if seq_score[i][4]:
                 write_out_data += "%s\t%s\t%s\t%s\t%.4f\t%d\t%d\t%s\n" % (genomic_location[i], "BEM", tf, seq_score[i][0], seq_score[i][1], seq_score[i][2], seq_score[i][3], seq_score[i][4])
#            else:
#                print("Found empty sequence")

        raw_out.write(write_out_data)
        raw_out.close()


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Not enough arguments supplied")
        sys.exit(1)

    from argparse import ArgumentParser

    # argument parser
    parser = ArgumentParser()
    parser = ArgumentParser(description='ENERGY arguments')
    parser.add_argument('--input', dest='data', type=str, help='the path to the input file')
    parser.add_argument('--pfm', dest='pfm', type=str, help='the path to the PFM to be used')
    parser.add_argument('--outdir', dest='out', type=str, help='the path to the output folder')
    parser.add_argument('--tf', dest='tf', type=str, help='the name of the transcription factor')
    parser.add_argument('--outfile', dest='outfile', type=str, help='the name of the output folder')
    args = parser.parse_args()

    run(args.data, args.pfm, args.out, args.tf.upper())
