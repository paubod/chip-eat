#!/bin/bash

source "../config.sh"

#parse arguments
in_dir=
if [ -z "$1" ]
    then
        echo "No input file specified. Exiting...";
        exit 1
    else
        in_dir=$1
fi

data_type=;
if [ -z "$2" ]
    then
        echo "No data type specified. Exiting...";
        exit 1
    else
        data_type=$2
fi

result_folder=
if [ -z "$3" ]
    then
        echo "No result folder specified. Exiting...";
        exit 1
    else
        result_folder=$3
fi

#optional arguments
tf_name=
if [ -z "$4" ]
    then
        echo "No TF name specified. Using TF...";
        tf_name="TF"
    else
        tf_name=$4
fi

# number of base pairs to increase on each side of the peak summit
window_size=
if [ -z "$5" ]
    then
        echo "No window size specified. Using 500bp";
        window_size=500
    else
        window_size=$5
fi
#number of base pairs to increase around peak summit for the training set
training_window_size=
if [ -z "$6" ]
    then
        echo "No training window size specified. Using 50bp";
        training_window_size=50
    else
        training_window_size=$6
fi

########## PARAMETER SETTING  ##################
bias_away_script=$GENERATE_BACKGROUND_SCRIPT
bias_away_repository=$BACKGROUND_REPOSITORY
hg_gen_file=$GENOME_FILE
hg_chr_file=$CHROMOSOME_FILE


#create folder structure and output file name
d=$in_dir #commodity only
# to be changed if needed
#out_dir=$result_folder/${d##*/}
out_dir=$result_folder'/training_files'
mkdir -p $out_dir

tf_name="$(echo "$tf_name" | tr '[:lower:]' '[:upper:]')"

out_file=$out_dir/${data_type}_${tf_name}_peaks

############ 1. Get peaks file depending on the data type ###############
  if [ $data_type = "MACS" ]
     then
       # echo "I'm in MACS";
        peaks_file=(`find ${d%/*}/${d##*/} -name "*_peaks.narrowPeak"`)
        peaks_file=${peaks_file[0]}
        if [ -e $peaks_file ]
           then
              ################ 1a. Add offset to chrStart #################
              awk -v OFS='\t' '{print $1,$2+$10,$2+$10+1,$4,$5,$6}' $peaks_file > $out_file
           else
              echo "Folder $d does not contain a results file. Check the MACS log file for more details. Skipping..";
        fi
  elif [ $data_type = "HOMER" ]
     then
       # echo "I'm in HOMER.. doh";
        peaks_file=${d%/*}/${d##*/}/${d##*/}_peaks.bed
        if [ -e $peaks_file ]
           then

              ############### 1a. Remove the header of the file with the parameter settings ################
              sed '/^#/ d' $peaks_file > ${peaks_file}_no_header

              ################ 1b. Calculate peak summit as the middle of the distance between start and end  #################
              awk -v OFS='\t' '{print $2,$3+int(($4-$3)/2), $3+int(($4-$3)/2+1),"'${d##*/}'",$9,$5}' ${peaks_file}_no_header > $out_file
           else
              echo "Folder $d does not contain a results file. Check the HOMER BED file for more details. Skipping..";
        fi
  elif [ $data_type = "BCP" ]
     then
       # echo "I'm in BCP";
        peaks_file=${d%/*}/${d##*/}/${d##*/}_peaks.bed
        if [ -e $peaks_file ]
           then
              ################ 1a. Put the peak summit as start and peak summit + 1 as end #################
              awk -v OFS='\t' '{print $1,$7,$7+1,"'${d##*/}'",$5,"."}' $peaks_file > $out_file
           else
              echo "Folder $d does not contain a results file. Check the BCP log file for more details. Skipping..";
        fi
  elif [ $data_type = "GEM" ]
     then
       # echo "I'm in GEM";
        #normally the ones from round 0 should be better, but they do not output it anymore, so we go for round 1 (read doc)
        peaks_file=${d%/*}/${d##*/}/${d##*/}/${d##*/}_outputs/${d##*/}_1.GEM_events.bed
        echo "Peak file: $peaks_file"
        if [ -e $peaks_file ]
           then
              ################ 1a. Add the peak summit offset to the start (always 100bp in the case of GEM) #################
              awk -v OFS='\t' '{print $1,$2+100,$2+101,"'${d##*/}'",$5,$6}' $peaks_file > $out_file
           else
              echo "Folder $d does not contain a results file. Check the GEM log file for more details. Skipping..";
        fi
  else
     echo "No supported data_type passed as argument. Only JAMM or MACS supported.";
     exit 1
  fi

  ################# 2. Increase area around peak max with 'window_size' upstream and downstream ################
  $PATH_TO_BEDTOOLS2/bedtools slop -i $out_file -g $hg_chr_file -b $window_size > ${out_file}_${window_size}bp
  # create TFFM training file: increase area around peak max with 50BP
  $PATH_TO_BEDTOOLS2/bedtools slop -i $out_file -g $hg_chr_file -b $training_window_size > ${out_file}_${training_window_size}bp
 
  #################  3. Replace the 4th column with a unique identifier. This is used internally by the DNAshaped_TFBS.py script
  #it is generated to respect the FASTA header format chr:start-end and use the coordinates in the result file
  awk -v OFS='\t' -F":" '$1=$1' ${out_file}_${window_size}bp | awk 'BEGIN {OFS="\t"} {$4 = $1":"$2"-"$3; print}' > ${out_file}_${window_size}bp.tmp
  mv ${out_file}_${window_size}bp.tmp ${out_file}_${window_size}bp
  # for the training file also (even though it should not be needed, only for the files on which the model is applied)
  awk -v OFS='\t' -F":" '$1=$1' ${out_file}_${training_window_size}bp | awk 'BEGIN {OFS="\t"} {$4 = $1":"$2"-"$3; print}' > ${out_file}_${training_window_size}bp.tmp
  mv ${out_file}_${training_window_size}bp.tmp ${out_file}_${training_window_size}bp
 
  rm $out_file
  foreground_file_bed=${out_file}_${training_window_size}bp
  out_file=${out_file}_${window_size}bp

  ################# 4. Map back to the genome and generate a FASTA file ##################
  $PATH_TO_BEDTOOLS2/fastaFromBed -fo ${out_file}.fa -fi $hg_gen_file -bed $out_file
  out_file=${out_file}.fa
  $PATH_TO_BEDTOOLS2/fastaFromBed -fo ${foreground_file_bed}.fa -fi $hg_gen_file -bed $foreground_file_bed
  foreground_file_fa=${foreground_file_bed}.fa

  ################# 5 Generate background FASTA file matching the GC composition of the foreground
 python2.7 $bias_away_script g -r $bias_away_repository -f $foreground_file_fa > ${foreground_file_fa}.background.fa
  background_file_fa=${foreground_file_fa}.background.fa

  ################ 6. Parse the FASTA file to obtain background BED files 
  # NOTE: we do not remove 1 from the start index as should be for the BED format (0 based) because fastaFromBed does not remove it either
  # if you want to respect the BED format pipe this: awk -v OFS='\t' '{print $1, $2-1, $3}' but it will crash the DNAshapedTFBS.py script
  # The 4th column holding the name is build to respect the FASTA file headers chr1:1000-10100
  awk '$0 ~ "^>"' $background_file_fa | awk '{gsub(">",""); print $1 }' | awk -v OFS='\t' -F":" '$1=$1' | awk -v OFS='\t' '{sub(/\-/,"\t",$2)};1' | awk 'BEGIN {OFS="\t"} {$4 = $1":"$2"-"$3; print}' > ${foreground_file_bed}.background

