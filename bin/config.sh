##### SET ACCORDING TO YOUR SYSTEM CONFIGURATION ########
#                                                       #
# This file should contain all paths to dependent tools #
# and other required files.                             #
#                                                       #
#########################################################


############### DEPENDENCIES LOCATION ######################

# download location of the ChIP-eat pipeline
CHIP_EAT_FOLDER=""

PATH_TO_BOWTIE2=""
PATH_TO_SAMTOOLS=""
PATH_TO_BEDTOOLS2=""

# absolute paths to the peak caller executable
# e.g., /home/user/HOMER/bin
PATH_TO_MACS=""
## optional peak callers
PATH_TO_BCP=""
PATH_TO_HOMER=""
PATH_TO_GEM=""

# maximum Java heap memory size for the GEM peak caller
JAVA_MEM=16G

# genome related for the mapping and coversion to FASTA
GENOME_FILE=""
CHROMOSOME_FILE=""
PATH_TO_BOWTIE_INDEX=""
PATH_TO_GB_SHAPE=""


# GB shape files - the DNA feature files (modify according to your file naming)
# HELT - Helix Twist 
# MGW - Minor Groove Width
# PROT - Propeller Twist
# ROLL - The Roll :|

#first order
HELT=$PATH_TO_GB_SHAPE"/hg38.HelT.wig.bw"
MGW=$PATH_TO_GB_SHAPE"/hg38.MGW.wig.bw"
PROT=$PATH_TO_GB_SHAPE"/hg38.ProT.wig.bw"
ROLL=$PATH_TO_GB_SHAPE"/hg38.Roll.wig.bw"
#second order
HELT2=$PATH_TO_GB_SHAPE"/hg38.HelT.2nd.wig.bw"
MGW2=$PATH_TO_GB_SHAPE"/hg38.MGW.2nd.wig.bw"
PROT2=$PATH_TO_GB_SHAPE"/hg38.ProT.2nd.wig.bw"
ROLL2=$PATH_TO_GB_SHAPE"/hg38.Roll.2nd.wig.bw"


############### INTERNAL TO THE PIPELINE ##############
# IMPORTANT: MODIFY ONLY IF YOU CHANGE THE DEFAULT LOCATION OF THE FILES
ALIGNMENT_SCRIPT=$CHIP_EAT_FOLDER"/bin/alignment/alignment.sh"
PEAK_CALLING_SCRIPT=$CHIP_EAT_FOLDER"/bin/peak_calling/peak_calling.sh"
PEAK_PROCESSING_SCRIPT=$CHIP_EAT_FOLDER"/bin/peak_processing/peak_processing.sh"

# needed by the peak processing scripts
PWM_SCORING_CODE=$CHIP_EAT_FOLDER"/bin/utils/pwm_searchPFF"
BEM_SCORING_CODE=$CHIP_EAT_FOLDER"/bin/utils/energy_searchPFF.py"
TFFM_MODULE=$CHIP_EAT_FOLDER"/src/TFFM-master"
TFFM_SCORING_CODE_FO=$CHIP_EAT_FOLDER"/bin/utils/TFFM_first_order.py"
TFFM_SCORING_CODE_FO_AND_DETAILED=$CHIP_EAT_FOLDER"/bin/utils/TFFM_first_order_and_detailed.py"
DIMO_TRAIN_SCODE=$CHIP_EAT_FOLDER"/bin/utils/train_DiMO.R"
DIMO_APPLY_SCODE=$CHIP_EAT_FOLDER"/bin/utils/call_DiMO.R"
DNA_SHAPED_CODE=$CHIP_EAT_FOLDER"/src/DNAshapedTFBS-master/DNAshapedTFBS.py"

# needed for TFFM, DiMO and DNAshaped. Generates background files matching the GC composition
# go through the READ.ME here https://github.com/wassermanlab/BiasAway to understand
GENERATE_BACKGROUND_SCRIPT=$CHIP_EAT_FOLDER"/src/BiasAway-noRPY/BiasAway.py"
BACKGROUND_REPOSITORY=$CHIP_EAT_FOLDER"/data/BiasAway_repository"
BACKGROUND_MAPPABLE_GENOME=$CHIP_EAT_FOLDER"/data/mappable_genome" 
BACKGROUND_UTILS=$CHIP_EAT_FOLDER"/bin/utils"
BACKGROUND_DUMMY_FILE=$BACKGROUND_UTILS"/dummy_file_bias_away.fa" #used in the repository generation
BACKGROUND_CALL_SEQUENCE_FILTERING=$BACKGROUND_UTILS"/call_filter_sequences.R"
BACKGROUND_SEQUENCE_FILTERING=$BACKGROUND_UTILS"/filter_sequences.R"
GENERATE_BACKGROUND_REPOSITORY=$BACKGROUND_UTILS"/create_background_repository.sh"
GENERATE_BACKGROUND_FILES=$BACKGROUND_UTILS"/generate_background_files.sh"

# defining the enrichment zone
COMPUTE_THRESHOLDS_SCRIPT=$CHIP_EAT_FOLDER"/bin/utils/compute_thresholds.R"
CALL_THRESHOLDS_SCRIPT=$CHIP_EAT_FOLDER"/bin/utils/call_thresholds.R"
ENTROPY_CODE=$CHIP_EAT_FOLDER"/bin/utils"

# the file containing the mapping between input ChIP-seq datasets, their TF and the PFM to use
TF_MAPPING=$CHIP_EAT_FOLDER"/bin/utils/TF_mapping.tsv"

#location of the folders containing the PFMs in different formats - used in the mapping process
PWM_FOLDER=$CHIP_EAT_FOLDER"/data/PWM_JASPAR"
MEME_FOLDER=$CHIP_EAT_FOLDER"/data/MEME_JASPAR"
PFM_FOLDER=$CHIP_EAT_FOLDER"/data/PFM_JASPAR"


