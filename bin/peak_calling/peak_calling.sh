#!/bin/bash

# This script will execute the peak calling part of the pipeline. It will process one set of 
# aligned reads by splitting them into controls and replicates, and call peaks using one of the
# four supported peak callers (MACS, BCP, HOMER and GEM). All replicates and all controls are
# separately concatenated and sorted prior to the peak calling. The genome reference file, 
# chromosome sizes and bowtie2 index paths should be set in the config.sh file.
#
# IMPORTANT: it assumes that each datasets contains replicate files **AND** control files. This 
# assumption is due to the implementation of each of the peak callers supported in the pipeline
# (i.e., some do not work without having control files). In order to identify which is which, 
# please refer to the documentation of the tools.
# To differentiate between replicates and controls, it assumes the string "control" is present in the name
# of the input control files.
#
# NOTE: that the parameter settings are specified in the diagrams present in the doc/ folder. 
# Most are set to default values. If you want to modify, refer to the documentation of each peak caller.
# 
# Dependencies: 
# -------------
# gzip/gunzip
# samtools
# bedtools2 
# java 1.8
#
# Peak callers:
# ------------
# MACS2: https://github.com/taoliu/MACS
# BCP: https://cb.utdallas.edu/BCP/
# HOMER: http://homer.ucsd.edu/homer/
# GEM: https://groups.csail.mit.edu/cgs/gem/
#
#
# usage bash peak_calling.sh /path/to/input/data/set /path/to/output/folder MACS
#
#


# load the paths of all dependencies 
# NOTE: it assumes that the config.sh file is in the bin folder of the pipeline
source "../config.sh"

usage="usage: bash peak_calling.sh /path/to/input/data/set /path/to/output/folder MACS"

# PARSE THE ARGUMENTS
in_dir=;
if [ -z "$1" ]
    then
        echo "No input file folder specified. Exiting..."
        echo $usage
        exit 1
    else
        in_dir=$1
fi

out_dir=;
if [ -z "$2" ]
    then
        echo "No output folder specified. Exiting..."
        echo $usage 
        exit 1
    else
        out_dir=$2
fi

# which peak caller to be used in processing
peak_caller=;
if [ -z "$3" ]
    then
        echo "No peak caller specified. Exiting..."
        echo $usage
        exit 1
    else
        peak_caller=$3
fi

# optional - if the messages should be displayed on the std
verbose=;
if [ ! -z $4 ]
  then
      verbose=$4
fi

# used to create delimited string from array
function join_by { local IFS="$1"; shift; echo "$*"; }

# Function to write to log. Note that if "verbose" is set,
# the log messages will be on the std also
logme () {
    if [ ! -z $verbose ]
       then
           echo "${*}" >> $log 2>&1;
    else
           echo "${*}" >> $log
    fi
}


# START PROCESSING
echo "Processing folder: ${in_dir##*/}"
# add current folder to the output path
out_dir=${out_dir}/${in_dir##*/}

#check if the output folder is not already there 
if [ ! -d "$out_dir" ]
     then

        #create folder structure
        mkdir -p $out_dir
        mkdir $out_dir/controls
        mkdir $out_dir/replicates     
    
        #split controls and replicates separately (ENCODE datasets have control in the name of the file)     
        for bam in $(find $in_dir -name "*.bam.gz")
           do
             if [[ "$(echo "$bam" | tr '[:upper:]' '[:lower:]')" == *"control"* ]]               
                then
                    cp $bam $out_dir/controls
                else
                    cp $bam $out_dir/replicates
            fi              
        done

        log=$out_dir/${in_dir##*/}.log;

        #check for controls and unzip them or log "no controls"
        all_ok=true;
        cd $out_dir/controls
        controls=(*)
        if [ ${#controls[@]} -eq 0 ]
            then
               logme "No control files present"
               all_ok=false;
            else
               gunzip *.gz
               controls=(*)
               #prefix with the absolute path
               controls=("${controls[@]/#/$out_dir/controls/}")
               controls=`join_by " " "${controls[@]}"`

               #merge all control files
               $PATH_TO_SAMTOOLS/samtools merge control.bam $controls
               #convert to BED
               $PATH_TO_BEDTOOLS2/bamToBed -i control.bam > control.bed

               #clean up
               rm *.bam
               $PATH_TO_BEDTOOLS2/sortBed -i control.bed > control.bed.sorted
               mv control.bed.sorted control.bed
        fi

        #check for replicates and unzip them or log "no replicates" 
        cd $out_dir/replicates
        replicates=(*)
        if [ ${#replicates[@]} -eq 0 ]
            then
               logme "No replicates present"
               all_ok=false;
            else
               gunzip *.gz;
               replicates=(*)
               #prefix with the absolute path       
               replicates=("${replicates[@]/#/$out_dir/replicates/}")
               replicates=`join_by " " "${replicates[@]}"`

               #merge all replicates files
               $PATH_TO_SAMTOOLS/samtools merge replicate.bam $replicates
               #convert to BED
               $PATH_TO_BEDTOOLS2/bamToBed -i replicate.bam > replicate.bed

               #clean up
               rm *.bam
               $PATH_TO_BEDTOOLS2/sortBed -i replicate.bed > replicate.bed.sorted
               mv replicate.bed.sorted replicate.bed
        fi

        # launch the peak calling by peak caller type
        if $all_ok
            then
               cd $out_dir

               if [ $peak_caller = "MACS" ]
                  then
                     logme "Started MACS..."
                     time $PATH_TO_MACS callpeak -t $out_dir/replicates/replicate.bed -c $out_dir/controls/control.bed -f BED -g hs -n ${in_dir##*/} >> $log 2>&1;
               elif [ $peak_caller = "BCP" ]
                  then                      
                     logme "Started BCP..." 
                     time $PATH_TO_BCP -1 $out_dir/replicates/replicate.bed -2 $out_dir/controls/control.bed -3 $out_dir/${in_dir##*/}_peaks.bed >> $log 2>&1;
              elif [ $peak_caller = "HOMER" ]
                  then
                     logme "Started HOMER..."
                     $PATH_TO_HOMER/makeTagDirectory $out_dir/replicates/tag $out_dir/replicates/replicate.bed -format bed >> $log 2>&1
                     $PATH_TO_HOMER/makeTagDirectory $out_dir/controls/tag $out_dir/controls/control.bed -format bed >> $log 2>&1;
                  time $PATH_TO_HOMER/findPeaks $out_dir/replicates/tag -center -style factor -o $out_dir/${in_dir##*/}_peaks.bed -i $out_dir/controls/tag >> $log 2>&1;
             elif [ $peak_caller = "GEM" ]
                  then
                     time java -Xmx"$JAVA_MEM" -jar $PATH_TO_GEM/gem.jar --d $PATH_TO_GEM/Read_Distribution_default.txt --g $CHROMOSOME_SIZE --genome $GENOME_FILE --expt $out_dir/replicates/replicate.bed --ctrl $out_dir/controls/control.bed --f BED --outBED --out $out_dir/${in_dir##*/} >> $log 2>&1;
             else
                 echo "Requested peak caller not supported. Only MACS, BCP, HOMER and GEM supported. Exiting..."
                 exit 1
       else
           logme "Not all needed input present."
           exit 1
       fi 
              
       #clean up
       rm -R $out_dir/controls;
       rm -R $out_dir/replicates;

else
    echo "Folder $in_dir already exists in results. Skipping...";
fi

