#!/bin/bash

# Will loop through all data sets in the input folder (passed as first argument) and
# call peaks for each of the subfolders in the input folder. It assumes the datasets
# are first level subfolders in the input directory. Specify an output folder as the 
# second argument. The peak caller of interest should be specified as the 3rd parameter
# and (optionally) specify the number of processes to be launched in parallel as the
# last argument. If not specified, it will use only one processor. 
#
# Dependencies:
# -------------
# bin/peak_calling/peak_calling.sh
# bin/config.sh
#
# NOTE that it assumes that the peak_calling script is located in the same folder as this script.
# If this is not the case, modify the config.sh file accordingly
#
# USAGE: bash launch_peak_calling.sh /path/to/input/folder/ /path/to/output/folder/ MACS [20]


# load the paths of all dependencies 
# NOTE: it assumes that the config.sh file is in the bin folder of the pipeline
source "../config.sh"

usage="bash launch_peak_calling.sh /my/input/folder /my/output/folder/ MACS 20"

#parse arguments
in_dir=;
if [ -z "$1" ]
    then 
	echo "No input file folder specified. Exiting..."; 
        echo "$usage"
	exit 1
    else	
	in_dir=$1
fi

out_dir=;
if [ -z "$2" ]
    then
        echo "No output folder specified. Exiting..."
        echo "$usage"
        exit 1
    else
        out_dir=$2
fi

# which peak caller to be used in processing
peak_caller=;
if [ -z "$3" ]
    then
        echo "No peak caller specified. Exiting..."
        echo "$usage"
        exit 1
    else
        peak_caller=$3
fi

#get how many processors to use in parallel for processing
processors=;
if [ -z "$4" ]
    then
        echo "No maximum number of processors to use was specified. Using 1 processor...";
        processors=1
    else
        processors=$4
fi


# count how many folder we got to process
nb_folders="$(ls -l $in_dir | grep -c ^d)"

#print parameter settings
echo "---------------------------------------"
echo "Input folder: $in_dir"
echo "Peak caller invoked: $peak_caller"
echo "Peak calling script: $PEAK_CALLING_SCRIPT"
echo "Bowtie index file: $PATH_TO_BOWTIE_INDEX"
echo "Genome file: $GENOME_FILE"
echo "Chromosome sizes: $CHROMOSOME_SIZE"
echo "Nb. folders to process: $nb_folders"
echo "---------------------------------------"


# LAUNCH PROCESSING

# get all subfolders at depth 1
find $in_dir -maxdepth 1 -mindepth 1 -type d | xargs -I {} --max-proc=$processors bash $PEAK_CALLING_SCRIPT {} $out_dir $peak_caller
