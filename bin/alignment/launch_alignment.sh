#!/bin/bash

# Will loop through all data sets in the input folder (passed as first argument) and
# call the alignment pipeline for each of the FASTQ files present in each subfolder
# of each data set. It assumes the datasets are first level subfolders in the input
# directory. The path to the bowtie index file for the reads alignment should be set
# in the config.sh file and (optionally) specify the number of processes to be launched
# in parallel as the second argument. If not specified, it will use only one processor. 
#
# Dependencies:
# -------------
# bin/alignment/alignment_pipeline.sh
# bin/config.sh
#
# NOTE that it assumes that the alignment script is located in the same folder as this script.
# If this is not the case, modify the config.sh file accordingly
#
# USAGE: bash launch_alignment.sh /path/to/input/folder/ [20]

# set all paths used across the pipeline
source "../config.sh"

usage="bash launch_alignment.sh /my/input/folder 20"

# PARSE ARGUMENTS

# the input folder containing the datasets
in_dir=;
if [ -z "$1" ]
    then 
	echo "No input file folder specified. Exiting..."; 
        echo "$usage"
	exit 1
    else	
	in_dir=$1
fi

#get how many processors to use in parallel for processing
processors=;
if [ -z "$2" ]
    then
        echo "No maximum number of processors to use was specified. Using 1 processor...";
        processors=1
    else
        processors=$2
fi


# count how many folder we got to process
nb_folders="$(ls -l $in_dir | grep -c ^d)"

#print parameter settings
echo "---------------------------------------"
echo "Input folder: $in_dir"
echo "Alignment script: $ALIGNMENT_SCRIPT"
echo "Bowtie index file: $PATH_TO_BOWTIE_INDEX"
echo "Nb. folders to process: $nb_folders"
echo "---------------------------------------"


# LAUNCH PROCESSING

# get inside the input folder and find all subfolders at depth 1
find $in_dir -maxdepth 1 -mindepth 1 -type d | xargs -I {} --max-proc=$processors bash $ALIGNMENT_SCRIPT {} > {}.log
