#!/bin/bash

# This script will execute the workflow needed to prepare the input data for the peak calling
# software. It assumes that there is at least one gzipped FASTQ file to be 
# processed in the input (sub)folder(s) passed as first argument. It will deal with single-ended
# and paired-ended sequences separately. To achieve this, it assumes that the paired-ended files
# have the pattern ENCFF795KJT_1_ENCFF067HZB.fastq.gz meaning that the file ENCFF795KJT is paired
# with ENCFF067HZB and that ENCFF795KJT is the first part of the pair. Check the file 
# paired_end_dataset_files.tsv for an ENCODE example mapping. The information about the order of 
# pairing can be (programmatically) retrieved in case of ENCODE data via the metadata.json file 
# that exists for each dataset. If the above described pattern does not exist in the file name, 
# it will assume the sequences are single-ended.
#
# More details about each step and the used parameters can be found in the diagram 
# "peak_calling_diagram.pdf" located in the doc folder.
#
# Dependencies: 
# -------------
# gzip/gunzip
# bowtie2
# samtools
# bedtools2 
#
# The script has been adapted from
# https://github.com/mahmoudibrahim/JAMM/wiki/ChIP-Seq-Alignment-and-Processing-Pipeline
#
#
# usage: bash alignment_pipeline.sh  /path/to/input/data/set
#
#
# NOTE that it assumes that the input files are gzipped (i.e., with a .gz extension) 
# and (again) that the paired-ended files have the pattern ENCFF795KJT_1_ENCFF067HZB.fastq.gz meaning that 
# the file ENCFF795KJT is paired with ENCFF067HZB and that ENCFF795KJT is the first part of the pair.
# Check the file paired_end_dataset_files.tsv for an ENCODE example mapping


# load paths to all dependencies
# NOTE that the file config.sh should be in the bin folder
source "../config.sh"

#parse arguments
in_dir=;
if [ -z "$1" ]
    then
        echo "No input file folder specified. Exiting...";
        exit 1
    else
        in_dir=$1
fi

if [ -z "$PATH_TO_BOWTIE_INDEX" ]
    then
        echo "No BOWTIE 2 index file specified. Check config.sh file. Exiting...";
        exit 1
fi


#process the data set
echo "Processing data set $in_dir";

  for fastq in $(find $in_dir -name "*.fastq.gz")
  	do 
	    if [ -e $fastq ]
	        then

               gunzip $fastq

               fastq=${fastq%.*}
#               echo "$fastq"

               #check if pair-ended or not (example file name ENCFF795KJT_1_ENCFF067HZB.fastq.gz)
	       fileIN=(${fastq//_/ })
               filename=${fileIN[0]} #get the file name
               order=${fileIN[1]} # get the order of a paired-ended FASTQ file
               paired_file=${fileIN[-1]%/*} #get last element which is the name of the paired file

               echo "Step 1: alignment using bowtie2";
               #no paired file
               if [ "$paired_file" == "$(dirname $fastq)" ]
		   then
             		$PATH_TO_BOWTIE2/bowtie2 -x $PATH_TO_BOWTIE_INDEX -U $fastq -S $fastq.sam 
                   else
                      #figure out the order of the pair-ended files
                      paired_file_order=1
                      fastqIsFirst=false
                      if [ "$order" == "$paired_file_order" ]
                         then
                             paired_file_order=2
                             fastqIsFirst=true
                      fi
                      #build up the file name of the pair
                      paired_file=$(dirname $fastq)/${paired_file%.*}_${paired_file_order}_$(basename $filename).fastq.gz
                      #unzip and remove extension from variable
		      gunzip $paired_file
                      paired_file=${paired_file%.*}

                      echo "The paired file for $fastq is $paired_file"
                      # check if the paired file exists in the folder or exit with error
                      if [ -e ${paired_file} ]
                          then
                              out=$(dirname $fastq)/$(basename $filename)_${fileIN[-1]%/*}.sam

                              if $fastqIsFirst
                                  then 
                                      $PATH_TO_BOWTIE2/bowtie2 -x $PATH_TO_BOWTIE_INDEX -1 $fastq -2 $paired_file -S $fastq.sam
#                                      echo "$PATH_TO_BOWTIE2/bowtie2 -x $PATH_TO_BOWTIE_INDEX -1 $fastq -2 $paired_file -S $fastq.sam"
                                      echo "Processed file ${out}.sam"
                                  else
                                      $PATH_TO_BOWTIE2/bowtie2 -x $PATH_TO_BOWTIE_INDEX -1 $paired_file -2 $fastq -S $fastq.sam
#                                      echo "$PATH_TO_BOWTIE2/bowtie2 -x $PATH_TO_BOWTIE_INDEX -1 $paired_file -2 $fastq -S $fastq.sam"
                                      echo "Processed file ${out}.sam" 
                              fi
                          else
                             echo "Paired file for $fastq does not exist for data set $(basename $in_dir)"
                             exit 1
                      fi

                      #remove the fastq files
                      rm $fastq
                      rm $paired_file
                             
               fi
             
		date

		echo "Step 2: filtering SAM";
		$PATH_TO_SAMTOOLS/samtools view -Sh $fastq.sam | \
		    grep -e "^@" -e "XM:i:[012][^0-9]" | \
		    grep -v "XS:i:" > $fastq.sam.filtered.sam
		date 

		echo "Step 3: SAM to BAM conversion"
		$PATH_TO_SAMTOOLS/samtools view -S -b $fastq.sam.filtered.sam \
		    > $fastq.sam.filtered.sam.bam
		$PATH_TO_SAMTOOLS/samtools sort -o $fastq.sam.filtered.sam.bam.sorted.bam \
		    $fastq.sam.filtered.sam.bam
		date

		echo "Step 4: remove PCR duplicates";
		$PATH_TO_SAMTOOLS/samtools rmdup -s $fastq.sam.filtered.sam.bam.sorted.bam \
		    $fastq.sam.filtered.sam.bam.sorted.bam.nodup.bam
		date
	
        	echo "Step 5: BAM indexing";
		$PATH_TO_SAMTOOLS/samtools index $fastq.sam.filtered.sam.bam.sorted.bam.nodup.bam
		date
	
		echo "Step 6: BAM to BED conversion";
		$PATH_TO_BEDTOOLS2/bedtools bamtobed \
		    -i $fastq.sam.filtered.sam.bam.sorted.bam.nodup.bam > \
		    $fastq.sam.filtered.sam.bam.sorted.bam.nodup.bam.bed
		date
	
        	echo "Step 7: compressing files";
		gzip $fastq.sam.filtered.sam.bam.sorted.bam.nodup.bam
		gzip $fastq.sam.filtered.sam.bam.sorted.bam.nodup.bam.bai
		gzip $fastq.sam.filtered.sam.bam.sorted.bam.nodup.bam.bed
		date

	    fi	
   done
